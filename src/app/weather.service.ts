import { catchError, map } from 'rxjs/operators';
import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Weather } from './interfaces/weather';
import { throwError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q="
  private KEY = "a7356d350754ccc09809b5da1a8084d8";
  private IMP = "units=metric";
  
  constructor(private http:HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server error');
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }
}
