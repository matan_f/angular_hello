// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyABd1R9A-NhsXZgCIprV3wsRb5U493lYdY",
    authDomain: "hello-71732.firebaseapp.com",
    databaseURL: "https://hello-71732.firebaseio.com",
    projectId: "hello-71732",
    storageBucket: "hello-71732.appspot.com",
    messagingSenderId: "754518717931",
    appId: "1:754518717931:web:a4a96ed3d8ddb5f52895ae"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
